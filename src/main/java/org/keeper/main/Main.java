/**
 * Main file.
 */

package org.keeper.main;

import static com.esotericsoftware.minlog.Log.LEVEL_TRACE;
import static com.esotericsoftware.minlog.Log.error;

import org.keeper.iot.wsn.plc.core.Core;
import org.keeper.iot.wsn.plc.resources.ResourceManagerSpawner;

import com.esotericsoftware.minlog.Log;

import java.net.SocketException;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class Main {
  /**
   * Blabla.
   * 
   * @param args Blabla.
   */
  public static void main(String[] args) {
    final String LOG_TAG = "main";
    Log.set(LEVEL_TRACE);

    Core core = null;

    try {
      core = new Core();
    } catch (SocketException e2) {
      error(LOG_TAG, "Error creating plc Core, socket error:" + e2.getMessage());
    }

    @SuppressWarnings("unused")
    ResourceManagerSpawner spawnr = new ResourceManagerSpawner(core);

    while (true) {
      try {
        Thread.sleep(10000);
      } catch (InterruptedException e) {
        spawnr = null;
        e.printStackTrace();
      }
    }
  }
}
