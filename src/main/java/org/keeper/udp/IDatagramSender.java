/*
 * Datagram sender interface.
 */

package org.keeper.udp;

import java.net.DatagramPacket;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public interface IDatagramSender {
  /**
   * Method sendPacket.
   * @param packet DatagramPacket
   */
  void sendPacket(DatagramPacket packet);
}
