package org.keeper.iot.wsn.plc.resources;

import static com.esotericsoftware.minlog.Log.trace;
import static com.esotericsoftware.minlog.Log.warn;

import java.util.LinkedList;
import java.util.List;

public class PlcResource {
  private static final String TAG = PlcResource.class.getSimpleName();

  /**
   * Prints the tree with the current node as it's root.
   * 
   * @param root Root node.
   * @param sb {@link StringBuilder} instance.
   * @param depth Indentation level.
   */
  public static void toStringHelper(PlcResource root, StringBuilder sb, int depth) {
    for (int i = depth; i > 0; --i) {
      sb.append("\t");
    }

    if (null != root.value) {
      sb.append(root.name + ":" + root.value + "\n");
    } else {
      sb.append(root.name + "\n");
    }

    if (null != root.children) {
      for (PlcResource resource : root.children) {
        toStringHelper(resource, sb, depth + 1);
      }
    }
  }

  private List<PlcResource> children = null;
  private final String name;
  private String value = null;
  public byte endpoint = 0;
  public byte clusterId = 0;
  public byte attributeId = 0;
  public byte datatype = 0;
  public byte datalen = 0;

  @SuppressWarnings("unused")
  private PlcResource() {
    name = null;
  }

  public PlcResource(String name) {
    this.name = name;
  }

  public PlcResource(String name, String value) {
    this.name = name;
    this.value = value;
  }

  public PlcResource(String name, byte endpoint, byte clusterId, byte attributeId, byte datatype,
      byte datalen) {
    this.name = name;
    this.endpoint = endpoint;
    this.clusterId = clusterId;
    this.attributeId = attributeId;
    this.datatype = datatype;
    this.datalen = datalen;
  }

  public PlcResource getChild(String[] path, int index) {
    for (PlcResource child : children) {
      trace(TAG, "Searching children, current is \"" + child.name + "\"");
      if (child.name.equalsIgnoreCase(path[index])) {
        if (index == path.length - 1) {
          return child;
        } else {
          return child.getChild(path, index + 1);
        }
      }
    }

    return null;
  }

  public PlcResource getChild(byte endpoint, byte clusterId, byte attributeId) {
    if (endpoint == this.endpoint && clusterId == this.clusterId && attributeId == this.attributeId) {
      return this;
    }

    if (null != children) {
      for (PlcResource child : children) {
        PlcResource res = child.getChild(endpoint, clusterId, attributeId);
        if (null != res) {
          trace(TAG, "Found child " + res.getName());
          return res;
        }
      }
    }

    return null;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    toStringHelper(this, sb, 0);
    return sb.toString();
  }

  public String getValue() {
    return value;
  }

  public String getName() {
    return name;
  }

  public void setValue(String value) {
    this.value = value;
    trace(TAG, "Set " + name + " value to \"" + value + "\"");
  }

  public void setValue(byte[] rawValue) {
    String val = ResourceHelpers.resourceToString(datatype, rawValue, datalen);
    if (null != val) {
      value = val;
    } else {
      value = "Error?";
    }
    trace(TAG, "Set " + name + " value to \"" + value + "\"");
  }

  /**
   * Adds a child resource if there isn't already a child resource with the same name.
   * 
   * @param child
   * @return
   */
  public boolean addChild(PlcResource child) {
    if (null == children) {
      children = new LinkedList<PlcResource>();
    }

    for (PlcResource _child : children) {
      if (_child.name.equals(child.name)) {
        warn(TAG, "Error adding child \"" + child.name + "\", other with same name exists.");
        return false;
      }
    }

    children.add(child);
    trace(TAG, "Added child node \"" + child.name + "\"");
    return true;
  }

  public void remoteSet(List<String> parameters) {
    warn(TAG, "Remote get not implemented.");
  }

  public void remoteGet() {
    warn(TAG, "Remote get not implemented.");
  }

  public void remoteGetAll() {
    remoteGet();
    
    if (null != children) {
      for (PlcResource child : children) {
        child.remoteGetAll();
      }
    }
  }
}
