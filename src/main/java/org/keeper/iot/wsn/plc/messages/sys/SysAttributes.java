package org.keeper.iot.wsn.plc.messages.sys;

import org.keeper.iot.wsn.plc.messages.DataTypes;

public class SysAttributes {
  /**
   * Length of manufacturer id.
   */
  public static final byte MANUF_ID_DATATYPE = DataTypes.DATATYPE_BYTE_ARRAY_ID;
  public static final int MANUF_ID_LEN = 20;
  /**
   * Length of model id.
   */
  public static final byte MODEL_DATATYPE = DataTypes.DATATYPE_BYTE_ARRAY_ID;
  public static final int MODEL_LEN = 20;
  /**
   * Size of serial.
   */
  public static final byte SERIAL_DATATYPE = DataTypes.DATATYPE_BYTE_ARRAY_ID;
  public static final int SERIAL_LEN = 8;
  /**
   * Size of date.
   */
  public static final byte DATE_DATATYPE = DataTypes.DATATYPE_BYTE_ARRAY_ID;
  public static final int DATE_LEN = 8;
  /**
   * Size of hardware version.
   */
  public static final byte HW_VER_DATATYPE = DataTypes.DATATYPE_BYTE_ARRAY_ID;
  public static final int HW_VER_LEN = 3;
  /**
   * Size of firmware version.
   */
  public static final byte FW_VER_DATATYPE = DataTypes.DATATYPE_BYTE_ARRAY_ID;
  public static final int FW_VER_LEN = 3;
  /**
   * Maximum chunk size.
   */
  public static final byte FW_CHUNK_DATATYPE = DataTypes.DATATYPE_BYTE_ARRAY_ID;
  public static final int FW_MAX_CHUNK_SIZE = 1024;
  /**
   * Maximum path length. ??
   */
  public static final int MAX_PATH_LENGTH = 250;

  public static final int PWRSRC_MAINS_SINGLE_PHASE = 0x01;
  public static final int PWRSRC_MAINS_3_PHASE = 0x02;
  public static final int PWRSRC_BATTERY = 0x04;
  public static final int PWRSRC_DC_SRC = 0x08;
  public static final int PWRSRC_BATT_BCK = 0x10;

  public static final int LOCAL_ACTION_REBOOT = 0;
  public static final int LOCAL_ACTION_JOIN = 1;
  public static final int LOCAL_ACTION_LEAVE = 2;
  public static final int LOCAL_ACTION_MODE1 = 3;
  public static final int LOCAL_ACTION_MODE2 = 4;
  public static final int LOCAL_ACTION_MODE3 = 5;
  public static final int LOCAL_ACTION_IDENTIFY = 6;
  public static final int LOCAL_ACTION_FACTORY_RESET = 7;
  public static final int LOCAL_ACTION_LEAVE_JOIN = 8;

  public static String getPowerSourceStr(byte powerSource) {
    StringBuilder sb = new StringBuilder();

    if ((powerSource & PWRSRC_MAINS_SINGLE_PHASE) != 0) {
      sb.append("Mains Single Phase ");
    }
    if ((powerSource & PWRSRC_MAINS_3_PHASE) != 0) {
      sb.append("Mains Three Phase ");
    }
    if ((powerSource & PWRSRC_BATTERY) != 0) {
      sb.append("Battery ");
    }
    if ((powerSource & PWRSRC_DC_SRC) != 0) {
      sb.append("External DC ");
    }
    if ((powerSource & PWRSRC_BATT_BCK) != 0) {
      sb.append("Battery Pack ");
    }

    return sb.toString();
  }

  public static String getLocalActionStr(int action) {
    switch (action) {
      case LOCAL_ACTION_REBOOT:
        return "Reboot";
      case LOCAL_ACTION_JOIN:
        return "PLC Join";
      case LOCAL_ACTION_LEAVE:
        return "PLC Leave";
      case LOCAL_ACTION_MODE1:
        return "Mode1";
      case LOCAL_ACTION_MODE2:
        return "Mode2";
      case LOCAL_ACTION_MODE3:
        return "Mode3";
      case LOCAL_ACTION_IDENTIFY:
        return "Identify";
      case LOCAL_ACTION_FACTORY_RESET:
        return "Factory reset";
      case LOCAL_ACTION_LEAVE_JOIN:
        return "Leave&Join";
      default:
        return "Error";
    }
  }
}
