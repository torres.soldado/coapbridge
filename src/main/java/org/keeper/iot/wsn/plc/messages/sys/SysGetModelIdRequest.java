/**
 * SysGetModelIdRequest.java
 * 
 * @author keeper
 */

package org.keeper.iot.wsn.plc.messages.sys;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;

import java.net.InetAddress;

/**
 * Implements the message builder for SysGetModelIdRequest.
 * 
 * @author keeper
 * @version $Revision: 1.0 $
 */
public class SysGetModelIdRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = SysGetModelIdRequest.class.getSimpleName();

  /**
   * Create message from raw data.
   * 
   * @param address Address of destination device.
   * 
   * @return Message.
   */
  public static Message createMessage(InetAddress address) {
    final byte[] messageData =
        new byte[] {0, 0, MessageFields.SOF, MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_SYS,
            Sys.SYS_CMD_GET_MODEL, 0};
    Message message = new SysGetModelIdRequest(address, messageData);

    return message;
  }

  /**
   * SysGetModelIdRequest.
   * 
   * @param address Address of destination device.
   * @param data Raw message data.
   */
  private SysGetModelIdRequest(InetAddress address, byte[] data) {
    super(address, data, SysGetModelIdResponse.REGISTRATION_ID);
  }

  /**
   * Method toString.
   * 
   * @return String
   */
  public String toString() {
    return LOG_TAG + super.toString();
  }
}
