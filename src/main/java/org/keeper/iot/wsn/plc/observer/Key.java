/**
 * Tries to implement an efficient hash for identifying plc messages.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.observer;

import static com.esotericsoftware.minlog.Log.trace;

import org.keeper.utils.Helpers;

import java.net.InetAddress;
import java.nio.ByteBuffer;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public final class Key {
  /**
   * A tag for the logger.
   */
  private static final String LOG_TAG = "Key";

  /**
   * A long which serves as a key for a map.
   */
  private final Long key;

  /**
   * Hash code.
   */
  private final int hash;

  /**
   * Designed for class A or B addresses. What it does it take the two least significant bytes from
   * the address and spreads them across the most significant nibbles of each byte of an integer.
   * The objective is to "exploit" the message id in which the most significant nibbles are rarely
   * if ever used.
   * 
   * @param addr Integer representing a class A or B Ip address in network order.
   * 
   * @return A int value to be used for hash.
   */
  private int addrToHash(int addr) {
    return ((addr & 0x0F) << 4) | ((addr & 0xF0) << 8) | ((addr & 0x0F00) << 12)
        | ((addr & 0xF000) << 16);
  }

  /**
   * Creates key with address.
   * 
   * @param address Address to use in key.
   */
  public Key(InetAddress address) {
    byte[] addr = address.getAddress();
    int addr32 =
        (addr[addr.length - 1] & 0xFF) | ((addr[addr.length - 2] & 0xFF) << 8)
            | ((addr[addr.length - 3] & 0xFF) << 16) | ((addr[addr.length - 4] & 0xFF) << 24);
    this.key = (Long.valueOf(addr32) << 32) & 0xFFFFFFFF00000000L;
    this.hash = addrToHash(addr32);
  }

  /**
   * Constructor for Key.
   * 
   * @param messageId int
   */
  public Key(int messageId) {
    this.key = Long.valueOf(messageId) & 0x00000000FFFFFFFFL;
    this.hash = messageId;
  }

  /**
   * Creates key with address and message id.
   * 
   * @param address Address to use in key.
   * @param messageId Message id to use in key.
   */
  public Key(InetAddress address, int messageId) {
    byte[] addr = address.getAddress();
    int addr32 =
        (addr[addr.length - 1] & 0xFF) | ((addr[addr.length - 2] & 0xFF) << 8)
            | ((addr[addr.length - 3] & 0xFF) << 16) | ((addr[addr.length - 4] & 0xFF) << 24);
    this.key =
        (Long.valueOf(addr32) << 32) & 0xFFFFFFFF00000000L
            | (Long.valueOf(messageId) & 0x00000000FFFFFFFFL);
    this.hash = messageId + addrToHash(addr32);
  }

  /**
   * Method equals.
   * 
   * @param obj Object
   * 
   * @return boolean
   */
  @Override
  public final boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    
    if (obj == null) {
      trace(LOG_TAG, "null");
      return false;
    }
    if (obj instanceof Key) {
      if (this.key.longValue() == ((Key) obj).key.longValue()) {
        // trace(LOG_TAG, "match found");
        return true;
      } else {
        trace(LOG_TAG, "not a match");
      }
    }
    return false;
  }

  /**
   * Method hashCode.
   * 
   * @return int
   */
  @Override
  public int hashCode() {
    // trace(LOG_TAG, "returning hashCode: " + Helpers.intToHexString(this.hash));
    return this.hash;
  }

  /**
   * Method toString.
   * 
   * @return String
   */
  @Override
  public String toString() {
    byte[] keyBytes = ByteBuffer.allocate(8).putLong(this.key.longValue()).array();
    return "Key(" + Helpers.arrayToHexString(keyBytes, keyBytes.length) + "), Hash("
        + Helpers.intToHexString(this.hash) + ")";
  }

}
