/**
 * SysGetManufacturerIdResponse.java
 * 
 * @author keeper
 */

package org.keeper.iot.wsn.plc.messages.sys;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class SysGetManufacturerIdResponse extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = SysGetManufacturerIdResponse.class.getSimpleName();
  /**
   * Used to register observer against this message Id.
   */
  public static final int REGISTRATION_ID = MessageFields.createId(new byte[] {0, 0,
      MessageFields.SOF, MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_SYS,
      Sys.SYS_CMD_GET_MANUF_ID, 0});

  /**
   * The PLC status bit-fields.
   */
  private final int status;

  /**
   * The device's manufacturer Id.
   */
  private final byte[] manufacturerId = new byte[SysAttributes.MANUF_ID_LEN];

  /**
   * Message parser.
   * 
   * @param address Address of device that originated this message.
   * @param data Raw message data from datagram.
   * 
   * @throws IllegalArgumentException if either parameter is null.
   */
  public SysGetManufacturerIdResponse(InetAddress address, byte[] data)
      throws IllegalArgumentException {
    super(address, data);
    this.status = (int) Helpers.getUnsigned32FromArrayLittleEndian(data, 5);
    Helpers.arrayToArray(data, this.manufacturerId, 9, SysAttributes.MANUF_ID_LEN, 0);
  }

  /**
   * Don't allow.
   */
  private SysGetManufacturerIdResponse() {
    super(null, null);
    this.status = 0;
  }

  /**
   * Calls the correct handler, the object must implement the Sys interface.
   * 
   * @param obs Object Observer on which the method will be called.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#callHandler(java.lang.Object)
   */
  @Override
  public void callHandler(Object obs) {
    if (obs instanceof Sys) {
      ((Sys) obs).onSysGetManufacturerIdResponse(super.getAddress(), this.status,
          this.manufacturerId);
    } else {
      MessageDebugHelpers.incompatibleHandler(LOG_TAG);
    }
  }

  /**
   * Method toString.
   * 
   * @return String
   */
  @Override
  public String toString() {
    return LOG_TAG + ": " + MessageDebugHelpers.printStatus(this.status) + " ManufacturerId("
        + Helpers.arrayToHexString(this.manufacturerId, this.manufacturerId.length) + ") "
        + super.toString();
  }
}
