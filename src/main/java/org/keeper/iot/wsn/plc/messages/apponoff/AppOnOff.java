package org.keeper.iot.wsn.plc.messages.apponoff;

import org.keeper.iot.wsn.plc.messages.DataTypes;

import java.net.InetAddress;

public interface AppOnOff {
  public static final byte ONOFF_CMD_GET_VERSION = 0;
  public static final byte ONOFF_CMD_ON = 1;
  public static final byte ONOFF_CMD_OFF = 2;
  public static final byte ONOFF_CMD_TOGGLE = 3;

  public static final byte ONOFF_ATT_RELAY_STATE_ID = 0;
  public static final byte ONOFF_ATT_RELAY_STATE_DATATYPE = DataTypes.DATATYPE_UINT8_ID;
  public static final byte ONOFF_ATT_RELAY_STATE_LENGTH = 1;
  public static final byte ONOFF_ATT_VAL_RELAY_STATE_OFF = 0;
  public static final byte ONOFF_ATT_VAL_RELAY_STATE_ON = 1;
  
  public static final byte ONOFF_ATT_RELAY_OP_MODE_ID = 1;
  public static final byte ONOFF_ATT_RELAY_OP_MODE_DATATYPE = DataTypes.DATATYPE_UINT8_ID;
  public static final byte ONOFF_ATT_RELAY_OP_MODE_LENGTH = 1;
  public static final byte ONOFF_ATT_VAL_RELAY_OP_MODE_OFF = 0;
  public static final byte ONOFF_ATT_VAL_RELAY_OP_MODE_ON = 1;
  public static final byte ONOFF_ATT_VAL_RELAY_OP_MODE_AUTO = 2;

  /**
   * Response with cluster version if the status is successful.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   * @param version Version of this cluster.
   * @param sequenceNumber Message sequence number.
   */
  public void onAppOnOffGetVersionResponse(InetAddress address, byte endpoint, int status,
      byte version, byte sequenceNumber);

  /**
   * Response.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   * @param version Version of this cluster.
   * @param sequenceNumber Message sequence number.
   */
  public void onAppOnOffRelayOnResponse(InetAddress address, byte endpoint, int status,
      byte sequenceNumber);

  /**
   * Response.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   * @param version Version of this cluster.
   * @param sequenceNumber Message sequence number.
   */
  public void onAppOnOffRelayOffResponse(InetAddress address, byte endpoint, int status,
      byte sequenceNumber);

  /**
   * Response.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   * @param version Version of this cluster.
   * @param sequenceNumber Message sequence number.
   */
  public void onAppOnOffRelayToggleResponse(InetAddress address, byte endpoint, int status,
      byte sequenceNumber);
}
