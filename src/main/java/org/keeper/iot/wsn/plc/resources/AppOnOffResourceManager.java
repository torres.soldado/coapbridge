package org.keeper.iot.wsn.plc.resources;

import static com.esotericsoftware.minlog.Log.debug;
import static com.esotericsoftware.minlog.Log.warn;

import org.keeper.iot.wsn.plc.core.ICore;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenericsGetAttributeRequest;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenericsGetAttributeResponse;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenericsSetAttributeResponse;
import org.keeper.iot.wsn.plc.messages.apponoff.AppOnOff;
import org.keeper.iot.wsn.plc.messages.apponoff.AppOnOffGetVersionRequest;
import org.keeper.iot.wsn.plc.messages.apponoff.AppOnOffGetVersionResponse;
import org.keeper.iot.wsn.plc.messages.apponoff.AppOnOffSetRelayOffRequest;
import org.keeper.iot.wsn.plc.messages.apponoff.AppOnOffSetRelayOnRequest;
import org.keeper.iot.wsn.plc.messages.apponoff.AppOnOffSetRelayToggleRequest;
import org.keeper.iot.wsn.plc.observer.IObserver;

import java.net.InetAddress;
import java.util.List;

public class AppOnOffResourceManager implements IResourceManager, IObserver, AppOnOff {
  /**
   * Decorate {@link HandlerAppGenerics}.
   */
  private class AppGenericsLocalHandler extends AppGenericsHandler {
    /**
     * @param parent The resource that contains this.
     */
    public AppGenericsLocalHandler(IResourceManager parent, String tag) {
      super(parent, tag);
    }

    @Override
    public void onAppGenericsGetAttributeResponse(InetAddress address, int status, byte endpoint,
        byte clusterId, byte attributeId, byte datatype, byte[] attributeData, byte sequenceNumber) {

      if (MessageDebugHelpers.isMessageForMeAndValidStatus(TAG,
          AppOnOffResourceManager.this.endpoint, endpoint, status)) {
        PlcResource resource = root.getChild(endpoint, clusterId, attributeId);
        if (null != resource) {
          if (resource.getName().equals("State")) {
            byte state = (byte) (attributeData[0] & 0x1);
            byte opMode = (byte) ((attributeData[0] & 0x6) >> 1);
            if (0 == state) {
              relayState.setValue("Off");
            } else {
              relayState.setValue("On");
            }
            
            if (0 == opMode) {
              relayOpMode.setValue("Manual Override Off");
            } else if (1 == opMode) {
              relayOpMode.setValue("Manual Override On");
            } else {
              relayOpMode.setValue("Software Controlled");
            }
          } else {
            resource.setValue(attributeData);
          }
        }
      }
    }
  }

  /**
   * Used by logger.
   */
  private static final String TAG = AppOnOffResourceManager.class.getSimpleName();

  /**
   * The target device's address.
   */
  private final InetAddress address;
  
  /**
   * The target endpoint.
   */
  private final byte endpoint;

  private final AppGenericsLocalHandler genericsHandler = new AppGenericsLocalHandler(this, TAG);
  private final PlcResource relayOpMode;
  private final PlcResource relayState;
  private final PlcResource resVersion;
  private final PlcResource root;

  public AppOnOffResourceManager(ICore core, InetAddress address, byte endpoint) {
    this.address = address;
    this.endpoint = endpoint;
    root = new PlcResource("OnOff");

    resVersion = new PlcResource("Version") {
      @Override
      public void remoteGet() {
        core.send(AppOnOffGetVersionRequest.createMessage(address, endpoint));
      }
    };
    core.subscribe(this, address, AppOnOffGetVersionResponse.getRegistrationId(endpoint));
    root.addChild(resVersion);

    relayState =
        new PlcResource("State", endpoint, MessageFields.APP_CLUSTER_ONOFF,
            AppOnOff.ONOFF_ATT_RELAY_STATE_ID, AppOnOff.ONOFF_ATT_RELAY_STATE_DATATYPE,
            AppOnOff.ONOFF_ATT_RELAY_STATE_LENGTH) {
          @Override
          public void remoteGet() {
            core.send(AppGenericsGetAttributeRequest.createMessage(address, endpoint, clusterId,
                attributeId, datatype, datalen));
          }

          @Override
          public void remoteSet(List<String> parameters) {
            String value = parameters.get(0);
            if (value.equalsIgnoreCase("on")) {
              core.send(AppOnOffSetRelayOnRequest.createMessage(address, endpoint));
            } else if (value.equalsIgnoreCase("off")) {
              core.send(AppOnOffSetRelayOffRequest.createMessage(address, endpoint));
            } else if (value.equalsIgnoreCase("toggle")) {
              core.send(AppOnOffSetRelayToggleRequest.createMessage(address, endpoint));
            } else {
              warn(TAG, "Invalid value (" + value + ") setting relay state");
            }
            debug(TAG, "Sent write attribute for " + this.getName() + " w/value " + value);
          }
        };
    core.subscribe(genericsHandler, address,
        AppGenericsGetAttributeResponse.getRegistrationId(endpoint));
    root.addChild(relayState);

    relayOpMode =
        new PlcResource("mode", endpoint, MessageFields.APP_CLUSTER_ONOFF,
            AppOnOff.ONOFF_ATT_RELAY_OP_MODE_ID, AppOnOff.ONOFF_ATT_RELAY_OP_MODE_DATATYPE,
            AppOnOff.ONOFF_ATT_RELAY_OP_MODE_LENGTH) {
//          @Override
//          public void remoteGet() {
//            core.send(AppGenericsGetAttributeRequest.createMessage(address, endpoint, clusterId,
//                attributeId, datatype, datalen));
//          }
        };
    core.subscribe(genericsHandler, address,
        AppGenericsSetAttributeResponse.getRegistrationId(endpoint));
    root.addChild(relayOpMode);
  }

  @Override
  public InetAddress getAddress() {
    return address;
  }

  @Override
  public PlcResource getChild(String[] path) {
    return root.getChild(path, 0);
  }

  @Override
  public PlcResource getRoot() {
    return root;
  }

  @Override
  public void onAppOnOffGetVersionResponse(InetAddress address, byte endpoint, int status,
      byte version, byte sequenceNumber) {
    /**
     * @BUG PM is responding badly to this message, SREQ instead of SRSP..
     */
//    if (MessageDebugHelpers.isMessageForMe(TAG, this.endpoint, endpoint, status)) {
      resVersion.setValue(String.valueOf(version));
//    }
  }

  @Override
  public void onAppOnOffRelayOffResponse(InetAddress address, byte endpoint, int status,
      byte sequenceNumber) {
    MessageDebugHelpers.isMessageForMeAndValidStatus(TAG, this.endpoint, endpoint, status);
  }

  @Override
  public void onAppOnOffRelayOnResponse(InetAddress address, byte endpoint, int status,
      byte sequenceNumber) {
    MessageDebugHelpers.isMessageForMeAndValidStatus(TAG, this.endpoint, endpoint, status);
  }

  @Override
  public void onAppOnOffRelayToggleResponse(InetAddress address, byte endpoint, int status,
      byte sequenceNumber) {
    MessageDebugHelpers.isMessageForMeAndValidStatus(TAG, this.endpoint, endpoint, status);
  }

  @Override
  public void onDeviceMessage(Message msg) {
    if (MessageDebugHelpers.isSameAddresses(TAG, this.address, msg.getAddress())) {
      msg.callHandler(this);
    }
  }

  @Override
  public void onDeviceMessageTimeout(int msgId) {
    MessageDebugHelpers.debugOnMessageTimeout(TAG, address, msgId);
  }

  @Override
  public void remoteSetAsync(String path, List<String> parameters) {
    MessageDebugHelpers.setResource(TAG, path, parameters, root);
  }

  @Override
  public void remoteUpdateAll() {
    root.remoteGetAll();
  }

}
