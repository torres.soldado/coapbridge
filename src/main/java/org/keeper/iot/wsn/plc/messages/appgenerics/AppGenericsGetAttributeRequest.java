/**
 * Creates a get version request {@link Message}.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.appgenerics;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.SequenceNumber;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class AppGenericsGetAttributeRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = AppGenericsGetAttributeRequest.class.getSimpleName();

  /**
   * Create message from raw data.
   * 
   * @param address Address of destination device.
   * @param endpoint Target endpoint.
   * 
   * @return Message.
   */
  public static Message createMessage(InetAddress address, byte endpoint, byte clusterId,
      byte attributeId, byte attributeDatatype, byte attributeLength) {
    final byte[] messageData =
        new byte[] {0, 0, MessageFields.SOF, MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_APP,
            MessageFields.APP_CLUSTER_GENERICS, AppGenerics.GEN_CMD_READ, endpoint, 1, clusterId,
            attributeId, attributeDatatype, attributeLength,
            (byte) SequenceNumber.getSequenceNumber(), 0};

    Message message =
        new AppGenericsGetAttributeRequest(address, messageData,
            AppGenericsGetAttributeResponse.getRegistrationId(endpoint));

    return message;
  }

  public byte getClusterId(byte[] data) {
    return data[8];
  }

  public byte getAttributeId(byte[] data) {
    return data[9];
  }

  public byte getAttributeDatatype(byte[] data) {
    return data[10];
  }

  public byte getAttributeDatalen(byte[] data) {
    return data[11];
  }

  /**
   * @param address Address of destination device.
   * @param data Raw message data.
   */
  private AppGenericsGetAttributeRequest(InetAddress address, byte[] data, int reponseId) {
    super(address, data, reponseId);
  }

  /**
   * Returns a string representation of message fields.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  public String toString() {
    byte clusterCmdId = MessageFields.getClusterCmdId(super.data);
    byte clusterId = getClusterId(super.data);
    byte attributeId = getAttributeId(super.data);
    byte attributeDataType = getAttributeDatatype(super.data);
    byte attributeDatalen = getAttributeDatalen(super.data);

    return LOG_TAG + ": " + super.toString() + ", cmdClusterId(" + Integer.toString(clusterCmdId)
        + "), endpoint(" + Integer.toString(MessageFields.getEndpoint(super.data))
        + "), clusterId(" + Integer.toString(clusterId) + "), attributeId("
        + Integer.toString(attributeId) + "), attributeDatatype("
        + Integer.toString(attributeDataType) + "), attributeDatalen("
        + Integer.toString(attributeDatalen) + "), sequenceNumer("
        + Integer.toString(MessageFields.getSequenceNumber(super.data)) + ")";
  }
}
