package org.keeper.iot.wsn.plc.messages.appsmarttemp;

import java.net.InetAddress;

public interface AppSmarttemp {
  public static final byte ST_CMD_GET_VERSION = (0);
  public static final byte ST_CMD_GET_CUR_TEMP = (1);
  public static final byte ST_CMD_GET_MAX_TEMP = (2);
  public static final byte ST_CMD_GET_MIN_TEMP = (3);

  /**
   * Response with cluster version if the status is successful.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   * @param version Version of this cluster.
   * @param sequenceNumber Message sequence number.
   */
  public void onAppSmarttempGetVersionResponse(InetAddress address, byte endpoint, int status,
      byte version, byte sequenceNumber);
  
  public void onAppSmarttempGetCurTempResponse(InetAddress address, byte endpoint, int status,
      byte temperature, byte sequenceNumber);
  
  public void onAppSmarttempGetMaxTempResponse(InetAddress address, byte endpoint, int status,
      byte temperature, byte sequenceNumber);
  
  public void onAppSmarttempGetMinTempResponse(InetAddress address, byte endpoint, int status,
      byte temperature, byte sequenceNumber);
}
