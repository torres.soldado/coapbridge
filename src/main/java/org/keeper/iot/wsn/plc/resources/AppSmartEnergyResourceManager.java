package org.keeper.iot.wsn.plc.resources;

import static com.esotericsoftware.minlog.Log.info;
import static com.esotericsoftware.minlog.Log.warn;

import org.keeper.iot.wsn.plc.core.ICore;
import org.keeper.iot.wsn.plc.messages.DataTypes;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenerics;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenericsGetAttributeRequest;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenericsGetAttributeResponse;
import org.keeper.iot.wsn.plc.messages.appsmartenergy.AppSmartEnergy;
import org.keeper.iot.wsn.plc.messages.appsmartenergy.AppSmartEnergyAttributes;
import org.keeper.iot.wsn.plc.messages.appsmartenergy.AppSmartEnergyGetMeterModeRequest;
import org.keeper.iot.wsn.plc.messages.appsmartenergy.AppSmartEnergyGetMeterModeResponse;
import org.keeper.iot.wsn.plc.messages.appsmartenergy.AppSmartEnergyGetVersionRequest;
import org.keeper.iot.wsn.plc.messages.appsmartenergy.AppSmartEnergyGetVersionResponse;
import org.keeper.iot.wsn.plc.messages.appsmartenergy.AppSmartEnergySetMeterModeRequest;
import org.keeper.iot.wsn.plc.observer.IObserver;

import java.net.InetAddress;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.naming.directory.InvalidAttributesException;

public class AppSmartEnergyResourceManager implements IResourceManager, IObserver, AppSmartEnergy {
  /**
   * Used by logger.
   */
  private static final String TAG = AppSmartEnergyResourceManager.class.getSimpleName();

  /**
   * The target device's address.
   */
  private final InetAddress address;

  /**
   * The target endpoint.
   */
  private final byte endpoint;

  private final AppGenericsLocalHandler genericsHandler = new AppGenericsLocalHandler(this, TAG);
  private final PlcResource resVersion;
  private final PlcResource resMeterMode;
  private final PlcResource root;

  /**
   * Decorate {@link HandlerAppGenerics}.
   */
  private class AppGenericsLocalHandler extends AppGenericsHandler {
    /**
     * @param parent The resource that contains this.
     */
    public AppGenericsLocalHandler(IResourceManager parent, String tag) {
      super(parent, tag);
    }

    @Override
    public void onAppGenericsGetAttributeResponse(InetAddress address, int status, byte endpoint,
        byte clusterId, byte attributeId, byte datatype, byte[] attributeData, byte sequenceNumber) {

      if (MessageDebugHelpers.isMessageForMeAndValidStatus(TAG,
          AppSmartEnergyResourceManager.this.endpoint, endpoint, status)) {
        PlcResource resource = root.getChild(endpoint, clusterId, attributeId);
        if (null != resource) {
          resource.setValue(attributeData);
        }
      }
    }
  }

  public AppSmartEnergyResourceManager(ICore core, InetAddress address, byte endpoint,
      AppGenerics.Cluster smartEnergyCluster) throws InvalidAttributesException {
    this.address = address;
    this.endpoint = endpoint;
    root = new PlcResource("SmartEnergy");

    resVersion = new PlcResource("Version") {
      @Override
      public void remoteGet() {
        core.send(AppSmartEnergyGetVersionRequest
            .createMessage(AppSmartEnergyResourceManager.this.address,
                AppSmartEnergyResourceManager.this.endpoint));
      }
    };
    core.subscribe(this, address, AppSmartEnergyGetVersionResponse.getRegistrationId(endpoint));
    root.addChild(resVersion);

    resMeterMode = new PlcResource("MeterMode") {
      @Override
      public void remoteGet() {
        core.send(AppSmartEnergyGetMeterModeRequest
            .createMessage(AppSmartEnergyResourceManager.this.address,
                AppSmartEnergyResourceManager.this.endpoint));
      }

      @Override
      public void remoteSet(List<String> parameters) {
        String value = parameters.get(0);
        byte[] mode =
            ResourceHelpers.resourceStringToRaw(DataTypes.DATATYPE_UINT8_ID, value,
                DataTypes.DATATYPE_UINT8_SIZE);
        if (mode[0] >= AppSmartEnergyAttributes.SE_OP_MODE_0
            && mode[0] <= AppSmartEnergyAttributes.SE_OP_MODE_2) {
          core.send(AppSmartEnergySetMeterModeRequest.createMessage(address, endpoint, mode[0]));
          info(TAG, "Remote set meter Mode" + value);
        } else {
          warn(TAG, "Can't set value for " + getName() + " invalid parameter:" + value);
        }
      }
    };
    core.subscribe(this, address, AppSmartEnergyGetMeterModeResponse.getRegistrationId(endpoint));

    for (Byte attributeId : smartEnergyCluster.attributes) {
      if (!ATTRIBUTE_NAMES_MAP.containsKey(attributeId)) {
        throw new InvalidAttributesException("unknown attribute id");
      }

      PlcResource resource =
          new PlcResource(ATTRIBUTE_NAMES_MAP.get(attributeId).name,
              AppSmartEnergyResourceManager.this.endpoint, MessageFields.APP_CLUSTER_SMARTENERGY,
              attributeId.byteValue(), ATTRIBUTE_NAMES_MAP.get(attributeId).datatype,
              DataTypes.dataTypeSize[ATTRIBUTE_NAMES_MAP.get(attributeId).datatype]) {
            @Override
            public void remoteGet() {
              core.send(AppGenericsGetAttributeRequest.createMessage(address, endpoint, clusterId,
                  attributeId, datatype, datalen));
            }
          };
      root.addChild(resource);
    }
    core.subscribe(genericsHandler, address,
        AppGenericsGetAttributeResponse.getRegistrationId(endpoint));
  }

  @Override
  public void onAppSmartEnergyGetVersionResponse(InetAddress address, byte endpoint, int status,
      byte version, byte sequenceNumber) {
    if (MessageDebugHelpers.isMessageForMe(TAG, this.endpoint, endpoint, status)) {
      resVersion.setValue(String.valueOf(version));
    }
  }

  @Override
  public void onAppSmartEnergySetMeterModeResponse(InetAddress address, byte endpoint, int status,
      byte sequenceNumber) {
    MessageDebugHelpers.isMessageForMeAndValidStatus(TAG, this.endpoint, endpoint, status);
  }

  @Override
  public void onAppSmartEnergyGetMeterModeResponse(InetAddress address, byte endpoint, int status,
      byte mode, byte sequenceNumber) {
    if (MessageDebugHelpers.isMessageForMeAndValidStatus(TAG, this.endpoint, endpoint, status)) {
      resMeterMode.setValue(String.valueOf(mode));
    }
  }

  @Override
  public void onDeviceMessage(Message msg) {
    if (MessageDebugHelpers.isSameAddresses(TAG, this.address, msg.getAddress())) {
      msg.callHandler(this);
    }
  }

  @Override
  public void onDeviceMessageTimeout(int msgId) {
    MessageDebugHelpers.debugOnMessageTimeout(TAG, address, msgId);
  }

  @Override
  public InetAddress getAddress() {
    return address;
  }

  @Override
  public PlcResource getChild(String[] path) {
    return root.getChild(path, 0);
  }

  @Override
  public PlcResource getRoot() {
    return root;
  }

  @Override
  public void remoteSetAsync(String path, List<String> parameters) {
    MessageDebugHelpers.setResource(TAG, path, parameters, root);
  }

  @Override
  public void remoteUpdateAll() {
    root.remoteGetAll();
  }

  /**
   * Field ATTRIBUTE_NAMES_MAP.
   */
  private static final Map<Byte, GenericAttribute> ATTRIBUTE_NAMES_MAP;
  static {
    final Map<Byte, GenericAttribute> map = new HashMap<Byte, GenericAttribute>(110, 1.0f);
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_C__A, new GenericAttribute("SE_ATT_E_A_C__A",
        AppSmartEnergyAttributes.SE_ATT_E_A_C__A_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_P__A, new GenericAttribute("SE_ATT_E_A_P__A",
        AppSmartEnergyAttributes.SE_ATT_E_A_P__A_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_P__Q1, new GenericAttribute("SE_ATT_E_R_P__Q1",
        AppSmartEnergyAttributes.SE_ATT_E_R_P__Q1_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_P__Q2, new GenericAttribute("SE_ATT_E_R_P__Q2",
        AppSmartEnergyAttributes.SE_ATT_E_R_P__Q2_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_N__Q3, new GenericAttribute("SE_ATT_E_R_N__Q3",
        AppSmartEnergyAttributes.SE_ATT_E_R_N__Q3_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_N__Q4, new GenericAttribute("SE_ATT_E_R_N__Q4",
        AppSmartEnergyAttributes.SE_ATT_E_R_N__Q4_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_C__AL1, new GenericAttribute("SE_ATT_E_A_C__AL1",
        AppSmartEnergyAttributes.SE_ATT_E_A_C__AL1_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_C__AL2, new GenericAttribute("SE_ATT_E_A_C__AL2",
        AppSmartEnergyAttributes.SE_ATT_E_A_C__AL2_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_C__AL3, new GenericAttribute("SE_ATT_E_A_C__AL3",
        AppSmartEnergyAttributes.SE_ATT_E_A_C__AL3_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_P__AL1, new GenericAttribute("SE_ATT_E_A_P__AL1",
        AppSmartEnergyAttributes.SE_ATT_E_A_P__AL1_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_P__AL2, new GenericAttribute("SE_ATT_E_A_P__AL2",
        AppSmartEnergyAttributes.SE_ATT_E_A_P__AL2_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_P__AL3, new GenericAttribute("SE_ATT_E_A_P__AL3",
        AppSmartEnergyAttributes.SE_ATT_E_A_P__AL3_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A__Q1Q4MD, new GenericAttribute("SE_ATT_P_A__Q1Q4MD",
        AppSmartEnergyAttributes.SE_ATT_P_A__Q1Q4MD_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A__Q1Q4MDCT, new GenericAttribute(
        "SE_ATT_P_A__Q1Q4MDCT", AppSmartEnergyAttributes.SE_ATT_P_A__Q1Q4MDCT_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A__Q2Q3MD, new GenericAttribute("SE_ATT_P_A__Q2Q3MD",
        AppSmartEnergyAttributes.SE_ATT_P_A__Q2Q3MD_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A__Q2Q3MDCT, new GenericAttribute(
        "SE_ATT_P_A__Q2Q3MDCT", AppSmartEnergyAttributes.SE_ATT_P_A__Q2Q3MDCT_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_P__R1C1A, new GenericAttribute(
        "SE_ATT_E_A_P__R1C1A", AppSmartEnergyAttributes.SE_ATT_E_A_P__R1C1A_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_P__R2C1A, new GenericAttribute(
        "SE_ATT_E_A_P__R2C1A", AppSmartEnergyAttributes.SE_ATT_E_A_P__R2C1A_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_P__R3C1A, new GenericAttribute(
        "SE_ATT_E_A_P__R3C1A", AppSmartEnergyAttributes.SE_ATT_E_A_P__R3C1A_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_P__R4C1A, new GenericAttribute(
        "SE_ATT_E_A_P__R4C1A", AppSmartEnergyAttributes.SE_ATT_E_A_P__R4C1A_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_P__R5C1A, new GenericAttribute(
        "SE_ATT_E_A_P__R5C1A", AppSmartEnergyAttributes.SE_ATT_E_A_P__R5C1A_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_P__R6C1A, new GenericAttribute(
        "SE_ATT_E_A_P__R6C1A", AppSmartEnergyAttributes.SE_ATT_E_A_P__R6C1A_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_P__TRC1A, new GenericAttribute(
        "SE_ATT_E_A_P__TRC1A", AppSmartEnergyAttributes.SE_ATT_E_A_P__TRC1A_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_N__R1C1A, new GenericAttribute(
        "SE_ATT_E_A_N__R1C1A", AppSmartEnergyAttributes.SE_ATT_E_A_N__R1C1A_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_N__R2C1A, new GenericAttribute(
        "SE_ATT_E_A_N__R2C1A", AppSmartEnergyAttributes.SE_ATT_E_A_N__R2C1A_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_N__R3C1A, new GenericAttribute(
        "SE_ATT_E_A_N__R3C1A", AppSmartEnergyAttributes.SE_ATT_E_A_N__R3C1A_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_N__R4C1A, new GenericAttribute(
        "SE_ATT_E_A_N__R4C1A", AppSmartEnergyAttributes.SE_ATT_E_A_N__R4C1A_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_N__R5C1A, new GenericAttribute(
        "SE_ATT_E_A_N__R5C1A", AppSmartEnergyAttributes.SE_ATT_E_A_N__R5C1A_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_N__R6C1A, new GenericAttribute(
        "SE_ATT_E_A_N__R6C1A", AppSmartEnergyAttributes.SE_ATT_E_A_N__R6C1A_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_A_N__TRC1A, new GenericAttribute(
        "SE_ATT_E_A_N__TRC1A", AppSmartEnergyAttributes.SE_ATT_E_A_N__TRC1A_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_P__R1C1Q1, new GenericAttribute(
        "SE_ATT_E_R_P__R1C1Q1", AppSmartEnergyAttributes.SE_ATT_E_R_P__R1C1Q1_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_P__R2C1Q1, new GenericAttribute(
        "SE_ATT_E_R_P__R2C1Q1", AppSmartEnergyAttributes.SE_ATT_E_R_P__R2C1Q1_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_P__R3C1Q1, new GenericAttribute(
        "SE_ATT_E_R_P__R3C1Q1", AppSmartEnergyAttributes.SE_ATT_E_R_P__R3C1Q1_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_P__R4C1Q1, new GenericAttribute(
        "SE_ATT_E_R_P__R4C1Q1", AppSmartEnergyAttributes.SE_ATT_E_R_P__R4C1Q1_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_P__R5C1Q1, new GenericAttribute(
        "SE_ATT_E_R_P__R5C1Q1", AppSmartEnergyAttributes.SE_ATT_E_R_P__R5C1Q1_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_P__R6C1Q1, new GenericAttribute(
        "SE_ATT_E_R_P__R6C1Q1", AppSmartEnergyAttributes.SE_ATT_E_R_P__R6C1Q1_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_P__TRC1Q1, new GenericAttribute(
        "SE_ATT_E_R_P__TRC1Q1", AppSmartEnergyAttributes.SE_ATT_E_R_P__TRC1Q1_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_P__R1C1Q2, new GenericAttribute(
        "SE_ATT_E_R_P__R1C1Q2", AppSmartEnergyAttributes.SE_ATT_E_R_P__R1C1Q2_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_P__R2C1Q2, new GenericAttribute(
        "SE_ATT_E_R_P__R2C1Q2", AppSmartEnergyAttributes.SE_ATT_E_R_P__R2C1Q2_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_P__R3C1Q2, new GenericAttribute(
        "SE_ATT_E_R_P__R3C1Q2", AppSmartEnergyAttributes.SE_ATT_E_R_P__R3C1Q2_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_P__R4C1Q2, new GenericAttribute(
        "SE_ATT_E_R_P__R4C1Q2", AppSmartEnergyAttributes.SE_ATT_E_R_P__R4C1Q2_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_P__R5C1Q2, new GenericAttribute(
        "SE_ATT_E_R_P__R5C1Q2", AppSmartEnergyAttributes.SE_ATT_E_R_P__R5C1Q2_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_P__R6C1Q2, new GenericAttribute(
        "SE_ATT_E_R_P__R6C1Q2", AppSmartEnergyAttributes.SE_ATT_E_R_P__R6C1Q2_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_P__TRC1Q2, new GenericAttribute(
        "SE_ATT_E_R_P__TRC1Q2", AppSmartEnergyAttributes.SE_ATT_E_R_P__TRC1Q2_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_N__R1C1Q3, new GenericAttribute(
        "SE_ATT_E_R_N__R1C1Q3", AppSmartEnergyAttributes.SE_ATT_E_R_N__R1C1Q3_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_N__R2C1Q3, new GenericAttribute(
        "SE_ATT_E_R_N__R2C1Q3", AppSmartEnergyAttributes.SE_ATT_E_R_N__R2C1Q3_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_N__R3C1Q3, new GenericAttribute(
        "SE_ATT_E_R_N__R3C1Q3", AppSmartEnergyAttributes.SE_ATT_E_R_N__R3C1Q3_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_N__R4C1Q3, new GenericAttribute(
        "SE_ATT_E_R_N__R4C1Q3", AppSmartEnergyAttributes.SE_ATT_E_R_N__R4C1Q3_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_N__R5C1Q3, new GenericAttribute(
        "SE_ATT_E_R_N__R5C1Q3", AppSmartEnergyAttributes.SE_ATT_E_R_N__R5C1Q3_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_N__R6C1Q3, new GenericAttribute(
        "SE_ATT_E_R_N__R6C1Q3", AppSmartEnergyAttributes.SE_ATT_E_R_N__R6C1Q3_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_N__TRC1Q3, new GenericAttribute(
        "SE_ATT_E_R_N__TRC1Q3", AppSmartEnergyAttributes.SE_ATT_E_R_N__TRC1Q3_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_N__R1C1Q4, new GenericAttribute(
        "SE_ATT_E_R_N__R1C1Q4", AppSmartEnergyAttributes.SE_ATT_E_R_N__R1C1Q4_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_N__R2C1Q4, new GenericAttribute(
        "SE_ATT_E_R_N__R2C1Q4", AppSmartEnergyAttributes.SE_ATT_E_R_N__R2C1Q4_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_N__R3C1Q4, new GenericAttribute(
        "SE_ATT_E_R_N__R3C1Q4", AppSmartEnergyAttributes.SE_ATT_E_R_N__R3C1Q4_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_N__R4C1Q4, new GenericAttribute(
        "SE_ATT_E_R_N__R4C1Q4", AppSmartEnergyAttributes.SE_ATT_E_R_N__R4C1Q4_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_N__R5C1Q4, new GenericAttribute(
        "SE_ATT_E_R_N__R5C1Q4", AppSmartEnergyAttributes.SE_ATT_E_R_N__R5C1Q4_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_N__R6C1Q4, new GenericAttribute(
        "SE_ATT_E_R_N__R6C1Q4", AppSmartEnergyAttributes.SE_ATT_E_R_N__R6C1Q4_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_E_R_N__TRC1Q4, new GenericAttribute(
        "SE_ATT_E_R_N__TRC1Q4", AppSmartEnergyAttributes.SE_ATT_E_R_N__TRC1Q4_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_P__R1C1MDLA, new GenericAttribute(
        "SE_ATT_P_A_P__R1C1MDLA", AppSmartEnergyAttributes.SE_ATT_P_A_P__R1C1MDLA_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_P__R1C1MDCT, new GenericAttribute(
        "SE_ATT_P_A_P__R1C1MDCT", AppSmartEnergyAttributes.SE_ATT_P_A_P__R1C1MDCT_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_P__R2C1MDLA, new GenericAttribute(
        "SE_ATT_P_A_P__R2C1MDLA", AppSmartEnergyAttributes.SE_ATT_P_A_P__R2C1MDLA_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_P__R2C1MDCT, new GenericAttribute(
        "SE_ATT_P_A_P__R2C1MDCT", AppSmartEnergyAttributes.SE_ATT_P_A_P__R2C1MDCT_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_P__R3C1MDLA, new GenericAttribute(
        "SE_ATT_P_A_P__R3C1MDLA", AppSmartEnergyAttributes.SE_ATT_P_A_P__R3C1MDLA_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_P__R3C1MDCT, new GenericAttribute(
        "SE_ATT_P_A_P__R3C1MDCT", AppSmartEnergyAttributes.SE_ATT_P_A_P__R3C1MDCT_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_P__R4C1MDLA, new GenericAttribute(
        "SE_ATT_P_A_P__R4C1MDLA", AppSmartEnergyAttributes.SE_ATT_P_A_P__R4C1MDLA_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_P__R4C1MDCT, new GenericAttribute(
        "SE_ATT_P_A_P__R4C1MDCT", AppSmartEnergyAttributes.SE_ATT_P_A_P__R4C1MDCT_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_P__R5C1MDLA, new GenericAttribute(
        "SE_ATT_P_A_P__R5C1MDLA", AppSmartEnergyAttributes.SE_ATT_P_A_P__R5C1MDLA_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_P__R5C1MDCT, new GenericAttribute(
        "SE_ATT_P_A_P__R5C1MDCT", AppSmartEnergyAttributes.SE_ATT_P_A_P__R5C1MDCT_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_P__R6C1MDLA, new GenericAttribute(
        "SE_ATT_P_A_P__R6C1MDLA", AppSmartEnergyAttributes.SE_ATT_P_A_P__R6C1MDLA_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_P__R6C1MDCT, new GenericAttribute(
        "SE_ATT_P_A_P__R6C1MDCT", AppSmartEnergyAttributes.SE_ATT_P_A_P__R6C1MDCT_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_P__TRC1MDLA, new GenericAttribute(
        "SE_ATT_P_A_P__TRC1MDLA", AppSmartEnergyAttributes.SE_ATT_P_A_P__TRC1MDLA_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_P__TRC1MDCT, new GenericAttribute(
        "SE_ATT_P_A_P__TRC1MDCT", AppSmartEnergyAttributes.SE_ATT_P_A_P__TRC1MDCT_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_N__R1C1MDLA, new GenericAttribute(
        "SE_ATT_P_A_N__R1C1MDLA", AppSmartEnergyAttributes.SE_ATT_P_A_N__R1C1MDLA_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_N__R1C1MDCT, new GenericAttribute(
        "SE_ATT_P_A_N__R1C1MDCT", AppSmartEnergyAttributes.SE_ATT_P_A_N__R1C1MDCT_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_N__R2C1MDLA, new GenericAttribute(
        "SE_ATT_P_A_N__R2C1MDLA", AppSmartEnergyAttributes.SE_ATT_P_A_N__R2C1MDLA_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_N__R2C1MDCT, new GenericAttribute(
        "SE_ATT_P_A_N__R2C1MDCT", AppSmartEnergyAttributes.SE_ATT_P_A_N__R2C1MDCT_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_N__R3C1MDLA, new GenericAttribute(
        "SE_ATT_P_A_N__R3C1MDLA", AppSmartEnergyAttributes.SE_ATT_P_A_N__R3C1MDLA_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_N__R3C1MDCT, new GenericAttribute(
        "SE_ATT_P_A_N__R3C1MDCT", AppSmartEnergyAttributes.SE_ATT_P_A_N__R3C1MDCT_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_N__R4C1MDLA, new GenericAttribute(
        "SE_ATT_P_A_N__R4C1MDLA", AppSmartEnergyAttributes.SE_ATT_P_A_N__R4C1MDLA_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_N__R4C1MDCT, new GenericAttribute(
        "SE_ATT_P_A_N__R4C1MDCT", AppSmartEnergyAttributes.SE_ATT_P_A_N__R4C1MDCT_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_N__R5C1MDLA, new GenericAttribute(
        "SE_ATT_P_A_N__R5C1MDLA", AppSmartEnergyAttributes.SE_ATT_P_A_N__R5C1MDLA_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_N__R5C1MDCT, new GenericAttribute(
        "SE_ATT_P_A_N__R5C1MDCT", AppSmartEnergyAttributes.SE_ATT_P_A_N__R5C1MDCT_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_N__R6C1MDLA, new GenericAttribute(
        "SE_ATT_P_A_N__R6C1MDLA", AppSmartEnergyAttributes.SE_ATT_P_A_N__R6C1MDLA_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_N__R6C1MDCT, new GenericAttribute(
        "SE_ATT_P_A_N__R6C1MDCT", AppSmartEnergyAttributes.SE_ATT_P_A_N__R6C1MDCT_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_N__TRC1MDLA, new GenericAttribute(
        "SE_ATT_P_A_N__TRC1MDLA", AppSmartEnergyAttributes.SE_ATT_P_A_N__TRC1MDLA_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_V_I__L1, new GenericAttribute("SE_ATT_V_I__L1",
        AppSmartEnergyAttributes.SE_ATT_V_I__L1_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_C_I__L1, new GenericAttribute("SE_ATT_C_I__L1",
        AppSmartEnergyAttributes.SE_ATT_C_I__L1_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_V_I__L2, new GenericAttribute("SE_ATT_V_I__L2",
        AppSmartEnergyAttributes.SE_ATT_V_I__L2_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_C_I__L2, new GenericAttribute("SE_ATT_C_I__L2",
        AppSmartEnergyAttributes.SE_ATT_C_I__L2_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_V_I__L3, new GenericAttribute("SE_ATT_V_I__L3",
        AppSmartEnergyAttributes.SE_ATT_V_I__L3_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_C_I__L3, new GenericAttribute("SE_ATT_C_I__L3",
        AppSmartEnergyAttributes.SE_ATT_C_I__L3_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_C_I__ALL, new GenericAttribute("SE_ATT_C_I__ALL",
        AppSmartEnergyAttributes.SE_ATT_C_I__ALL_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_R_P__Q1, new GenericAttribute("SE_ATT_P_R_P__Q1",
        AppSmartEnergyAttributes.SE_ATT_P_R_P__Q1_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_R_P__Q2, new GenericAttribute("SE_ATT_P_R_P__Q2",
        AppSmartEnergyAttributes.SE_ATT_P_R_P__Q2_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_R_P__Q2, new GenericAttribute("SE_ATT_P_R_P__Q2",
        AppSmartEnergyAttributes.SE_ATT_P_R_P__Q2_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_R_N__Q3, new GenericAttribute("SE_ATT_P_R_N__Q3",
        AppSmartEnergyAttributes.SE_ATT_P_R_N__Q3_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_R_N__Q4, new GenericAttribute("SE_ATT_P_R_N__Q4",
        AppSmartEnergyAttributes.SE_ATT_P_R_N__Q4_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_P__L1, new GenericAttribute("SE_ATT_P_A_P__L1",
        AppSmartEnergyAttributes.SE_ATT_P_A_P__L1_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_N__L1, new GenericAttribute("SE_ATT_P_A_N__L1",
        AppSmartEnergyAttributes.SE_ATT_P_A_N__L1_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_P__L2, new GenericAttribute("SE_ATT_P_A_P__L2",
        AppSmartEnergyAttributes.SE_ATT_P_A_P__L2_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_N__L2, new GenericAttribute("SE_ATT_P_A_N__L2",
        AppSmartEnergyAttributes.SE_ATT_P_A_N__L2_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_P__L3, new GenericAttribute("SE_ATT_P_A_P__L3",
        AppSmartEnergyAttributes.SE_ATT_P_A_P__L3_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_N__L3, new GenericAttribute("SE_ATT_P_A_N__L3",
        AppSmartEnergyAttributes.SE_ATT_P_A_N__L3_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_P__ALL, new GenericAttribute("SE_ATT_P_A_P__ALL",
        AppSmartEnergyAttributes.SE_ATT_P_A_P__ALL_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P_A_N__ALL, new GenericAttribute("SE_ATT_P_A_N__ALL",
        AppSmartEnergyAttributes.SE_ATT_P_A_N__ALL_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P__PF, new GenericAttribute("SE_ATT_P__PF",
        AppSmartEnergyAttributes.SE_ATT_P__PF_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P__PFL1, new GenericAttribute("SE_ATT_P__PFL1",
        AppSmartEnergyAttributes.SE_ATT_P__PFL1_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P__PFL2, new GenericAttribute("SE_ATT_P__PFL2",
        AppSmartEnergyAttributes.SE_ATT_P__PFL2_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_P__PFL3, new GenericAttribute("SE_ATT_P__PFL3",
        AppSmartEnergyAttributes.SE_ATT_P__PFL3_DATATYPE));
    map.put(AppSmartEnergyAttributes.SE_ATT_F, new GenericAttribute("SE_ATT_F",
        AppSmartEnergyAttributes.SE_ATT_F_DATATYPE));
    ATTRIBUTE_NAMES_MAP = Collections.unmodifiableMap(map);
  }
}
