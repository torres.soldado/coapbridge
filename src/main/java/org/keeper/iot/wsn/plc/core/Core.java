package org.keeper.iot.wsn.plc.core;

import static com.esotericsoftware.minlog.Log.error;
import static com.esotericsoftware.minlog.Log.trace;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.observer.IObserver;
import org.keeper.iot.wsn.plc.observer.PlcBroker;
import org.keeper.iot.wsn.plc.router.PlcMessageRouter;
import org.keeper.udp.IDatagramSender;
import org.keeper.udp.UdpEndpoint;

import java.net.InetAddress;
import java.net.SocketException;

/**
 * Joins all components.
 */
public class Core implements ICore {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = Core.class.getSimpleName();

  /**
   * PlcBroker takes care of notifying the observers.
   */
  private final PlcBroker broker = new PlcBroker();
  /**
   * PlcMessageRouter takes care of forwarding messages to each device.
   */
  private final PlcMessageRouter router;
  /**
   * Just a udp endpoint listener.
   */
  public final IDatagramSender udpEndpoint;

  /**
   * Instantiates Plc client core.
   * 
   * @throws SocketException If there is an error creating the socket used to receive/send
   *         datagrams to the plc network.
   **/
  public Core() throws SocketException {
    trace(LOG_TAG, "constructor");

    router = new PlcMessageRouter(broker);

    try {
      udpEndpoint = new UdpEndpoint(router, Configurables.PLC_SERVER_PORT).start();
    } catch (SocketException e) {
      error(LOG_TAG, "Creating udp endpoint");
      throw e;
    }

    router.setDatagramSender(udpEndpoint);
  }

  @Override
  protected Object clone() throws CloneNotSupportedException {
    throw new CloneNotSupportedException();
  }
  
  @Override
  public String toString() {
    return "Plc cliente core, sergio.soldado@withus.pt";
  }
  
  /**
   * Method subscribe.
   * 
   * @param obs IObserver
   * @param addr InetAddress
   * @param msgId int
   */
  @Override
  public void subscribe(IObserver obs, InetAddress addr, int msgId) {
    broker.registerObserver(obs, addr, msgId, false);
  }

  /**
   * Method subscribe.
   * 
   * @param obs IObserver
   * @param msgId int
   */
  @Override
  public void subscribe(IObserver obs, int msgId) {
    broker.registerObserver(obs, msgId, false);
  }

  /**
   * Method unsubscribe.
   * 
   * @param obs IObserver
   * @param addr InetAddress
   * @param msgId int
   */
  @Override
  public void unsubscribe(IObserver obs, InetAddress addr, int msgId) {
    broker.removeObserver(obs, addr, msgId);
  }

  /**
   * Method unsubscribe.
   * 
   * @param obs IObserver
   * @param msgId int
   */
  @Override
  public void unsubscribe(IObserver obs, int msgId) {
    broker.removeObserver(obs, msgId);
  }

  /**
   * Method send.
   * 
   * @param msg Message
   */
  @Override
  public void send(Message msg) {
    router.enqueue(msg);
  }

  /**
   * Method sendAndSubscribeOneShot.
   * 
   * @param obs IObserver
   * @param msg Message
   */
  @Override
  public void sendAndSubscribeOneShot(IObserver obs, Message msg) {
    broker.registerObserver(obs, msg.getAddress(), msg.getResponseId(), true);
    send(msg);
  }
}
