/**
 * Response interface.
 */

package org.keeper.iot.wsn.plc.messages;

import org.keeper.iot.wsn.plc.messages.nwk.Nwk;
import org.keeper.iot.wsn.plc.messages.nwk.NwkAnnounceResponse;

/**
 * @author sergio.soldado@withus.pt
 *
 * @version $Revision: 1.0 $
 */
public interface IResponse {
  /**
   * Each subclass calls a specific interface method that the object should implement. For instance
   * a given class is handled a {@link NwkAnnounceResponse}, it then calls this method on the
   * message which in turn calls the specific method as defined in {@link Nwk}, in this case it
   * would be o.onOnNwkAnnounceAReq(..).
   * 
   * @param obs Object on which a subclass specific method is to be called.
   */
  void callHandler(Object obs);
}
