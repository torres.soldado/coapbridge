/**
 * Very simple sequence number.
 */

package org.keeper.iot.wsn.plc.messages;

import org.keeper.utils.Helpers;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author sergio.soldado@withus.pt
 *
 * @version $Revision: 1.0 $
 */
public final class SequenceNumber {
  /**
   * Not used.
   * 
   * @return Empty string.
   */
  @Override
  public String toString() {
    return "";
  }

  /**
   * Not used.
   * 
   * 
   * @return Throws exception before returning.
   * @throws CloneNotSupportedException because cloning this object is not allowed.
   */
  @Override
  public Object clone() throws CloneNotSupportedException {
    throw new CloneNotSupportedException("Not allowed");
  }

  /**
   * Current sequence number. Incremented after each use.
   */
  private static final AtomicInteger SequenceNum = new AtomicInteger(Helpers.getRandomInt(0, 255));

  /**
   * Increments and returns current sequence number.
   * 
   * 
   * @return Message sequence number.
   */
  public static byte getSequenceNumber() {
    return (byte) SequenceNum.incrementAndGet();
  }
}
