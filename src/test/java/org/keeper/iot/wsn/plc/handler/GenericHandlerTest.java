package org.keeper.iot.wsn.plc.handler;

import static com.esotericsoftware.minlog.Log.LEVEL_TRACE;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.keeper.iot.wsn.plc.core.ICore;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenericsGetCapabilitiesResponse;
import org.keeper.iot.wsn.plc.observer.IObserver;

import com.esotericsoftware.minlog.Log;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.naming.directory.InvalidAttributesException;

public class GenericHandlerTest {
  class CustomCore implements ICore {
    Message lastMessageSent = null;

    public CustomCore() {}

    @Override
    public void subscribe(IObserver obs, InetAddress addr, int msgId) {}

    @Override
    public void subscribe(IObserver obs, int msgId) {}

    @Override
    public void unsubscribe(IObserver obs, InetAddress addr, int msgId) {}

    @Override
    public void unsubscribe(IObserver obs, int msgId) {}

    @Override
    public void send(Message msg) {
      lastMessageSent = msg;
    }

    @Override
    public void sendAndSubscribeOneShot(IObserver obs, Message msg) {}

  }

  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    Log.set(LEVEL_TRACE);
  }

  @Test
  public void testGenericHandler() {
    byte[] data =
        {0x6b, 0x00, (byte) 0xfe, 0x65, 0x00, 0x07, 0x01, 0x00, 0x00, 0x00, 0x02, 0x03, 0x01, 0x02,
            0x05, 0x07, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x04, 0x00, 0x01, 0x04, 0x05,
            0x00, 0x01, 0x00, 0x02, 0x00, 0x01, 0x03, 0x00, 0x01, 0x02, 0x02, 0x00, 0x03, 0x00,
            0x0e, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x5c, 0x5d, 0x5e, 0x5f, 0x60, 0x67, 0x68,
            0x6d, 0x02, 0x00, 0x03, 0x00, 0x07, 0x06, 0x09, 0x56, 0x57, 0x61, 0x62, 0x6a, 0x02,
            0x00, 0x03, 0x00, 0x07, 0x07, 0x0a, 0x58, 0x59, 0x63, 0x64, 0x6b, 0x02, 0x00, 0x03,
            0x00, 0x07, 0x08, 0x0b, 0x5a, 0x5b, 0x65, 0x66, 0x6c, 0x02, 0x00, 0x02, 0x00, 0x02,
            0x00, 0x01, 0x02, 0x00, 0x02, 0x00, 0x02, 0x00, 0x01, 0x00, (byte) 0xfc};

    InetAddress address = null;
    try {
      address = InetAddress.getByName("192.168.100.1");
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Should not happen");
    }

    AppGenericsGetCapabilitiesResponse response =
        new AppGenericsGetCapabilitiesResponse(address, data);

    CustomCore core = new CustomCore();
    GenericHandler handler = null;
    try {
      handler = new GenericHandler(response.getCapabilities(), core, address);
    } catch (InvalidAttributesException e) {
      e.printStackTrace();
      fail("Should not happen");
    }
    System.out.println("Resources: " + handler.getRootNode().toString());
  }

}
