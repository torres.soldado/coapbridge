package org.keeper.iot.wsn.plc.messages.sys;

import static com.esotericsoftware.minlog.Log.LEVEL_TRACE;

import com.esotericsoftware.minlog.Log;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.utils.Helpers;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class SysGetModelIdResponseTest {
  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    Log.set(LEVEL_TRACE);
  }

  class CustomHandler implements Sys {
    public int status = 0;
    public InetAddress address = null;
    public byte[] modelId = null;

    @Override
    public void onSysGetVersionResponse(InetAddress address, int status, byte version) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysGetManufacturerIdResponse(InetAddress address, int status,
        byte[] manufacturerId) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysGetModelIdResponse(InetAddress address, int status, byte[] modelId) {
      this.address = address;
      this.status = status;
      this.modelId = modelId;
    }

    @Override
    public void onSysGetHardwareRevisionResponse(InetAddress address, int status, byte[] hwVersion) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysGetSerialResponse(InetAddress address, int status, byte[] serial) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysGetFirmwareVersionResponse(InetAddress address, int status, byte[] fwVersion) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysGetDateResponse(InetAddress address, int status, byte[] date) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysSetDateResponse(InetAddress address, int status) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysUpdateFirmwareResponse(InetAddress address, int status) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysRebootResponse(InetAddress address, int status) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysGetTemperature(InetAddress address, int status, byte temperatureCelsius) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysGetPowerSource(InetAddress address, int status, byte powerSource) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysFactoryReset(InetAddress address, int status) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysUserDidSomething(InetAddress address, int status, int actions) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysFirmwareChunkUpload(InetAddress address, int status) {
      fail("Shouldn't happen");
    }

  }

  @Test
  public void test() {
    InetAddress address = null;
    try {
      address = InetAddress.getByName("192.168.100.1");
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Should not happen");
    }

    byte[] data =
        {0x1c, 0x00, (byte) 0xfe, 0x61, 0x02, 0x01, 0x00, 0x00, 0x00, 0x50, 0x4d, 0x20, 0x35, 0x31,
            0x34, 0x37, 0x3a, 0x35, 0x33, 0x32, 0x37, 0x4d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, (byte) 0xd2, 0x00, 0x00}; // captured from udp, 2 bogus bytes (from logger
                                            // probably).

    byte[] data2 =
        {0x1c, 0x00, (byte) 0xfe, 0x61, 0x02, 0x01, 0x00, 0x00, 0x00, 0x50, 0x4d, 0x20, 0x35, 0x31,
            0x34, 0x37, 0x3a, 0x35, 0x33, 0x32, 0x37, 0x4d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, (byte) 0xd2}; // without bogus bytes

    byte[] modId =
        {0x50, 0x4d, 0x20, 0x35, 0x31, 0x34, 0x37, 0x3a, 0x35, 0x33, 0x32, 0x37, 0x4d, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00};

    Message msg = new SysGetModelIdResponse(address, data);

    assertTrue(Helpers.compareArrays(data2, msg.getRaw()));
    assertTrue(MessageDebugHelpers.isSameAddresses("test", address, msg.getAddress()));

    CustomHandler handler = new CustomHandler();
    msg.callHandler(handler);

    assertTrue(MessageDebugHelpers.isSameAddresses("test", handler.address, msg.getAddress()));
    assertEquals(1, handler.status);
    assertTrue(Helpers.compareArrays(modId, handler.modelId));
  }

}
