package org.keeper.iot.wsn.plc.handler.resources;

import static com.esotericsoftware.minlog.Log.LEVEL_TRACE;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.keeper.iot.wsn.plc.messages.DataTypes;
import org.keeper.iot.wsn.plc.resources.ResourceHelpers;
import org.keeper.utils.Helpers;

import com.esotericsoftware.minlog.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ResourceHelpersTest {
  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    Log.set(LEVEL_TRACE);
  }
  
  // Following values in LITTLE-ENDIAN
  byte[] int8Example = {-100}; // -100
  String int8ExampleStr = "-100";
  byte[] uint8Example = {(byte) 255}; // 255
  String uint8ExampleStr = "255";
  byte[] int16Example = {(byte) 0xFF, (byte) 0xFF}; // -1
  String int16ExampleStr = "-1";
  byte[] uint16Example = {0, (byte) 0x80}; // 32768
  String uint16ExampleStr = "32768";
  byte[] int32Example = {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF}; // -1
  String int32ExampleStr = "-1";
  byte[] uint32Example = {(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x80}; // 2,147,483,648
  String uint32ExampleStr = "2147483648";
  byte[] int64Example = {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
      (byte) 0xFF, (byte) 0xFF, (byte) 0xFF}; // -1
  String int64ExampleStr = "-1";
  byte[] uint64Example = {(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
      (byte) 0x00, (byte) 0x00, (byte) 0x80}; // 9.22337203685e+18
  String uint64ExampleStr = "9223372036854775808";
  float floatEx = 1.123f;
  byte[] floatExample = ByteBuffer.allocate(4).putFloat(floatEx).order(ByteOrder.LITTLE_ENDIAN)
      .array();
  String floatExampleStr = "1.123";
  double doubleEx = 0.000244140625f;
  byte[] doubleExample = ByteBuffer.allocate(8).putDouble(doubleEx).order(ByteOrder.LITTLE_ENDIAN)
      .array();
  String doubleExampleStr = "2.44140625E-4";
  byte[] arrayExample = {'s', 'o', 'm', 'e', 't', 'h', 'i', 'n', 'g'};
  String arrayExampleStr = "something";

  @Test
  public void testResourceToString() {
    String int8ExampleString =
        ResourceHelpers.resourceToString(DataTypes.DATATYPE_INT8_ID, int8Example,
            int8Example.length);
    assertEquals(int8ExampleStr, int8ExampleString);
    System.out.println("int8ExampleString: " + int8ExampleString);

    String uint8ExampleString =
        ResourceHelpers.resourceToString(DataTypes.DATATYPE_UINT8_ID, uint8Example,
            uint8Example.length);
    assertEquals(uint8ExampleStr, uint8ExampleString);
    System.out.println("uint8ExampleString: " + uint8ExampleString);

    String int16ExampleString =
        ResourceHelpers.resourceToString(DataTypes.DATATYPE_INT16_ID, int16Example,
            int16Example.length);
    assertEquals(int16ExampleStr, int16ExampleString);
    System.out.println("int16ExampleString: " + int16ExampleString);

    String uint16ExampleString =
        ResourceHelpers.resourceToString(DataTypes.DATATYPE_UINT16_ID, uint16Example,
            uint16Example.length);
    assertEquals(uint16ExampleStr, uint16ExampleString);
    System.out.println("uint16ExampleString: " + uint16ExampleString);

    String int32ExampleString =
        ResourceHelpers.resourceToString(DataTypes.DATATYPE_INT32_ID, int32Example,
            int32Example.length);
    assertEquals(int32ExampleStr, int32ExampleString);
    System.out.println("int32ExampleString: " + int32ExampleString);

    String uint32ExampleString =
        ResourceHelpers.resourceToString(DataTypes.DATATYPE_UINT32_ID, uint32Example,
            uint32Example.length);
    assertEquals("2147483648", uint32ExampleString);
    System.out.println("uint32ExampleString: " + uint32ExampleString);

    String int64ExampleString =
        ResourceHelpers.resourceToString(DataTypes.DATATYPE_INT64_ID, int64Example,
            int64Example.length);
    assertEquals(int64ExampleStr, int64ExampleString);
    System.out.println("int64ExampleString: " + int64ExampleString);

    String uint64ExampleString =
        ResourceHelpers.resourceToString(DataTypes.DATATYPE_UINT64_ID, uint64Example,
            uint64Example.length);
    assertEquals(uint64ExampleStr, uint64ExampleString);
    System.out.println("uint64ExampleString: " + uint64ExampleString);

    String floatExampleString =
        ResourceHelpers.resourceToString(DataTypes.DATATYPE_FLOAT_ID, floatExample,
            floatExample.length);
    assertEquals(floatExampleStr, floatExampleString);
    System.out.println("floatExampleString: " + floatExampleString);

    String doubleExampleString =
        ResourceHelpers.resourceToString(DataTypes.DATATYPE_DOUBLE_ID, doubleExample,
            doubleExample.length);
    assertEquals(doubleExampleStr, doubleExampleString);
    System.out.println("doubleExampleString: " + doubleExampleString);

    String arrayExampleString =
        ResourceHelpers.resourceToString(DataTypes.DATATYPE_BYTE_ARRAY_ID, arrayExample,
            arrayExample.length);
    assertEquals(arrayExampleStr, arrayExampleString);
    System.out.println("arrayExampleString: " + arrayExampleString);
  }

  @Test
  public void testResourceStringToRaw() {
    byte[] int8Val =
        ResourceHelpers.resourceStringToRaw(DataTypes.DATATYPE_INT8_ID, int8ExampleStr,
            DataTypes.DATATYPE_INT8_SIZE);
    assertTrue(Helpers.compareArrays(int8Val, int8Example));

    byte[] uint8Val =
        ResourceHelpers.resourceStringToRaw(DataTypes.DATATYPE_UINT8_ID, uint8ExampleStr,
            DataTypes.DATATYPE_UINT8_SIZE);
    assertTrue(Helpers.compareArrays(uint8Val, uint8Example));

    byte[] int16Val =
        ResourceHelpers.resourceStringToRaw(DataTypes.DATATYPE_INT16_ID, int16ExampleStr,
            DataTypes.DATATYPE_INT16_SIZE);
    assertTrue(Helpers.compareArrays(int16Val, int16Example));

    byte[] uint16Val =
        ResourceHelpers.resourceStringToRaw(DataTypes.DATATYPE_UINT16_ID, uint16ExampleStr,
            DataTypes.DATATYPE_UINT16_SIZE);
    assertTrue(Helpers.compareArrays(uint16Val, uint16Example));
    
    byte[] int32Val =
        ResourceHelpers.resourceStringToRaw(DataTypes.DATATYPE_INT32_ID, int32ExampleStr,
            DataTypes.DATATYPE_INT32_SIZE);
    assertTrue(Helpers.compareArrays(int32Val, int32Example));
    
    byte[] uint32Val =
        ResourceHelpers.resourceStringToRaw(DataTypes.DATATYPE_UINT32_ID, uint32ExampleStr,
            DataTypes.DATATYPE_UINT32_SIZE);
    assertTrue(Helpers.compareArrays(uint32Val, uint32Example));
    
    byte[] int64Val =
        ResourceHelpers.resourceStringToRaw(DataTypes.DATATYPE_INT64_ID, int64ExampleStr,
            DataTypes.DATATYPE_INT64_SIZE);
    assertTrue(Helpers.compareArrays(int64Val, int64Example));
    
    byte[] uint64Val =
        ResourceHelpers.resourceStringToRaw(DataTypes.DATATYPE_UINT64_ID, uint64ExampleStr,
            DataTypes.DATATYPE_UINT64_SIZE);
    assertTrue(Helpers.compareArrays(uint64Val, uint64Example));
    
    byte[] floatVal =
        ResourceHelpers.resourceStringToRaw(DataTypes.DATATYPE_FLOAT_ID, floatExampleStr,
            DataTypes.DATATYPE_FLOAT_SIZE);
    assertTrue(Helpers.compareArrays(floatVal, floatExample));
    
    byte[] doubleVal =
        ResourceHelpers.resourceStringToRaw(DataTypes.DATATYPE_DOUBLE_ID, doubleExampleStr,
            DataTypes.DATATYPE_DOUBLE_SIZE);
    assertTrue(Helpers.compareArrays(doubleVal, doubleExample));
    
    byte[] arrayVal =
        ResourceHelpers.resourceStringToRaw(DataTypes.DATATYPE_BYTE_ARRAY_ID, arrayExampleStr,
            arrayExampleStr.length());
    assertTrue(Helpers.compareArrays(arrayVal, arrayExample));
  }

}
