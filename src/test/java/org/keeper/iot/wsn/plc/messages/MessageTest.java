/**
 * 
 */
package org.keeper.iot.wsn.plc.messages;

import static com.esotericsoftware.minlog.Log.LEVEL_TRACE;

import static org.junit.Assert.*;

import com.esotericsoftware.minlog.Log;

import org.junit.BeforeClass;
import org.junit.Test;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.dummy.DummyRequest;
import org.keeper.iot.wsn.plc.messages.dummy.DummyResponseMessage;
import org.keeper.iot.wsn.plc.messages.nwk.Nwk;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author keeper
 *
 */
public class MessageTest {

  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    Log.set(LEVEL_TRACE);
  }

  /**
   * Test method for {@link org.keeper.iot.wsn.plc.messages.Message#Message(java.net.InetAddress, byte[])}.
   */
  @Test(expected = java.lang.IllegalArgumentException.class)
  public void testMessageNullAddress() {
    Message msg = new Message(null, new byte[] {0}) {
      @Override
      public void callHandler(Object obs) {}
    };
    
    assertNotNull(msg);
  }

  @Test(expected = java.lang.IllegalArgumentException.class)
  public void testMessageNullData() {
    InetAddress addr = null;
    try {
      addr = InetAddress.getByName("192.168.100.1");
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }

    Message msg = new Message(addr, null) {
      @Override
      public void callHandler(Object obs) {}
    };
    
    assertNotNull(msg);
  }

  @Test(expected = java.lang.IllegalArgumentException.class)
  public void testMessageNull() {
    Message msg = new Message(null, null) {
      @Override
      public void callHandler(Object obs) {}
    };
    
    assertNotNull(msg);
  }

  /**
   * Test method for {@link org.keeper.iot.wsn.plc.messages.Message#equals(java.lang.Object)}.
   */
  @Test
  public void testEqualsObject() {
    byte[] data0 =
        new byte[] {00, 00, MessageFields.SOF,
            MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_NWK, Nwk.NWK_INVALID, 0};
    MessageFields.setLenAuto(data0);
    MessageFields.setChecksumAuto(data0);
    byte[] data1 =
        new byte[] {00, 00, MessageFields.SOF,
            MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_NWK, Nwk.NWK_INVALID, 0};
    MessageFields.setLenAuto(data1);
    MessageFields.setChecksumAuto(data1);
    byte[] data2 =
        new byte[] {00, 00, MessageFields.SOF,
            MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_NWK, Nwk.NWK_INVALID, 1, 0};
    MessageFields.setLenAuto(data2);
    MessageFields.setChecksumAuto(data2);

    InetAddress address0 = null;
    InetAddress address1 = null;
    InetAddress address2 = null;
    try {
      address0 = InetAddress.getByName("192.168.100.1");
      address1 = InetAddress.getByName("192.168.100.1");
      address2 = InetAddress.getByName("192.168.100.2");
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }

    Message msg0 = new Message(address0, data0) {
      @Override
      public void callHandler(Object obs) {}
    };

    Message msg1 = new Message(address1, data1) {
      @Override
      public void callHandler(Object obs) {}
    };

    Message msg2 = new Message(address0, data2) {
      @Override
      public void callHandler(Object obs) {}
    };

    Message msg3 = new Message(address2, data0) {
      @Override
      public void callHandler(Object obs) {}
    };

    assertEquals(msg0, msg1);
    assertNotEquals(msg0, msg2);
    assertNotEquals(msg1, msg3);
  }

  /**
   * Test method for {@link org.keeper.iot.wsn.plc.messages.Message#getAddress()}.
   */
  @Test
  public void testGetAddress() {
    byte[] data0 =
        new byte[] {00, 00, MessageFields.SOF,
            MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_NWK, Nwk.NWK_INVALID, 0};
    MessageFields.setLenAuto(data0);
    MessageFields.setChecksumAuto(data0);
    InetAddress address0 = null;
    try {
      address0 = InetAddress.getByName("192.168.100.1");
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }

    Message msg0 = new Message(address0, data0) {
      @Override
      public void callHandler(Object obs) {}
    };

    assertEquals(address0, msg0.getAddress());
  }

  /**
   * Test method for {@link org.keeper.iot.wsn.plc.messages.Message#getChecksum()}.
   */
  @Test
  public void testGetChecksum() {
    byte[] data0 = new byte[] {00, 00, MessageFields.SOF, 0, 0, 0};
    MessageFields.setLenAuto(data0);
    MessageFields.setChecksumAuto(data0);
    InetAddress address0 = null;
    try {
      address0 = InetAddress.getByName("192.168.100.1");
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }

    Message msg0 = new Message(address0, data0) {
      @Override
      public void callHandler(Object obs) {}
    };

    assertEquals(MessageFields.SOF, msg0.getChecksum());
  }

  /**
   * Test method for {@link org.keeper.iot.wsn.plc.messages.Message#getId()}.
   */
  @Test
  public void testGetId() {
    byte[] data0 = new byte[] {00, 00, MessageFields.SOF, 0, 0, 0};
    MessageFields.setLenAuto(data0);
    MessageFields.setChecksumAuto(data0);
    byte[] data1 =
        new byte[] {00, 00, MessageFields.SOF,
            MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_NWK, Nwk.NWK_INVALID, 0};
    MessageFields.setLenAuto(data1);
    MessageFields.setChecksumAuto(data1);
    InetAddress address0 = null;
    try {
      address0 = InetAddress.getByName("192.168.100.1");
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }

    Message msg0 = new Message(address0, data0) {
      @Override
      public void callHandler(Object obs) {}
    };

    assertEquals(0x0F0F, msg0.getId());

    Message msg1 = new Message(address0, data1) {
      @Override
      public void callHandler(Object obs) {}
    };

    int expectedId =
        ((MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_NWK) << 24) | (Nwk.NWK_INVALID << 16)
            | 0x0F0F;
    assertEquals(expectedId, msg1.getId());
  }

  /**
   * Test method for {@link org.keeper.iot.wsn.plc.messages.Message#getLength()}.
   */
  @Test
  public void testGetLength() {
    byte[] data0 = new byte[] {00, 00, MessageFields.SOF, 0, 0, 0};
    MessageFields.setLenAuto(data0);
    MessageFields.setChecksumAuto(data0);

    InetAddress address0 = null;
    try {
      address0 = InetAddress.getByName("192.168.100.1");
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }

    Message msg0 = new Message(address0, data0) {
      @Override
      public void callHandler(Object obs) {}
    };

    assertEquals(4, msg0.getLength());
  }

  /**
   * Test method for {@link org.keeper.iot.wsn.plc.messages.Message#getRaw()}.
   */
  @Test
  public void testGetPayload() {
    byte[] data0 =
        new byte[] {00, 00, MessageFields.SOF,
            MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_NWK, Nwk.NWK_INVALID, 0};
    MessageFields.setLenAuto(data0);
    MessageFields.setChecksumAuto(data0);

    InetAddress address0 = null;
    try {
      address0 = InetAddress.getByName("192.168.100.1");
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }

    Message msg0 = new Message(address0, data0) {
      @Override
      public void callHandler(Object obs) {}
    };

    byte[] payload = msg0.getRaw();
    if (data0.length != payload.length) {
      fail("Arrays are different");
    }

    for (int i = 0; i < data0.length; ++i) {
      if (data0[i] != payload[i]) {
        fail("Arrays are different");
      }
    }
  }

  /**
   * Test method for {@link org.keeper.iot.wsn.plc.messages.Message#getResponseId()}.
   */
  @Test
  public void testGetResponseId() {
    InetAddress address0 = null;
    try {
      address0 = InetAddress.getByName("192.168.100.1");
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }

    Message msg = DummyRequest.createMessage(address0);

    assertEquals(DummyResponseMessage.REGISTRATION_ID, msg.getResponseId());
  }

}
